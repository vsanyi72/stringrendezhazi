package vs.stringrendez;

import java.util.Arrays;

/**
 * A Sring tömbben szereplő elemek rendezése abc sorrendben
 * 
 * A feladatot List használata nélkül, tömbökkel kell megcsinálni!
 * 
 */
public class StringRendez {
	// TODO: Beadandó házi feladat!
	
	
	

	// TODO: Beadandó házi feladat!
		
		
		public static void main(String[] args) {
	
			// "Körte"
			// "Alma"
			// "Barack"
	
			String[] gyumolcsok = { "Körte", "alma", "Barack", "cseresznye", "Banán" };
	
			// String gyumolcs = "Cseresznye";
			// System.out.println( (int)gyumolcs.charAt(0) );
			
			System.out.println("A gyümölcsök: " + Arrays.toString(gyumolcsok) + "]");
			
			// seged mátrix
			int[][] matrix = new int[gyumolcsok.length][14];
	
			for (int i = 0; i < gyumolcsok.length; i++) {
				String gyumolcs = gyumolcsok[i].toLowerCase();
				gyumolcsok[i]=gyumolcs;
				int num = (int) gyumolcs.charAt(0);
			}
			System.out.println("A gyümölcsök kis kezdőbetűvel: " + Arrays.toString(gyumolcsok) + "]");
			// mátrix feltöltése
			for (int i = 0; i < matrix.length; i++) {
				for (int j = 0; j < gyumolcsok[i].length(); j++) {
					matrix[i][j] = gyumolcsok[i].charAt(j);
				}
			}
			System.out.println("Mátrix rendezés előtt: ");
			System.out.println(Arrays.deepToString(matrix));
	
			// rendezés
	
	// ki lehetne emelni egy metódusba
			int hossz = matrix.length;
			for (int i = 0; i < hossz - 1; i++)
				for (int j = 0; j < hossz - i - 1; j++)
					if (matrix[j][0] > matrix[j + 1][0]) {
	// csere - ez is lehetne metódus
						int temp = 0;
						int k = 0;
						while ((matrix[j][k] != 0)||(matrix[j+1][k] != 0)) {
							temp = matrix[j][k];
							matrix[j][k] = matrix[j + 1][k];
							matrix[j + 1][k] = temp;
							k++;
						}
	
					}
	
			System.out.println("Mátrix rendezés után");
			System.out.println(Arrays.deepToString(matrix));
	// kiirás, visszaalakítás
	
	// visszaalakítás
			String[] rendezettGyumolcsok = new String[11];
			for (int i = 0; i < matrix.length; i++) {
				String temp = "";
				int k=0;
				while (matrix[i][k] != 0)  {
					temp += (char) matrix[i][k];
					k++;
				}
				rendezettGyumolcsok[i] = temp;
			}
	// kiirás
			System.out.println("A rendezett gyümölcsök: " + Arrays.toString(rendezettGyumolcsok) + "]");
	
		}

	
		
		
	}

