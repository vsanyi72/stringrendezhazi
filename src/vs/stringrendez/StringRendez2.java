package vs.stringrendez;

import java.util.Arrays;

/**
 * A Sring tömbben szereplő elemek rendezése abc sorrendben
 * 
 * A feladatot List használata nélkül, tömbökkel kell megcsinálni!
 * 
 */
public class StringRendez2 {

	// TODO: Beadandó házi feladat!

	// Mátrix nélküli megvalósítás,

	public static void main(String[] args) {

		// "Körte"
		// "Alma"
		// "Barack"

		String[] gyumolcsok = { "Körte", "alma", "Barack", "cseresznye", "Banán" };

		// String gyumolcs = "Cseresznye";
		// System.out.println( (int)gyumolcs.charAt(0) );

		System.out.println("A gyümölcsök: " + Arrays.toString(gyumolcsok) + "]");

		for (int i = 0; i < gyumolcsok.length; i++) {
			String gyumolcs = gyumolcsok[i].toLowerCase();
			gyumolcsok[i] = gyumolcs;
		}
		System.out.println("A gyümölcsök kis kezdőbetűvel: " + Arrays.toString(gyumolcsok) + "]");

		// rendezés

		// ki lehetne emelni egy metódusba
		int hossz = gyumolcsok.length;
		for (int i = 0; i < hossz - 1; i++) {
			for (int j = 0; j < hossz - i - 1; j++) {
				if (gyumolcsok[j].charAt(0) > gyumolcsok[j + 1].charAt(0)) {
					// csere - ez is lehetne metódus
					String temp = "";
					temp = gyumolcsok[j];
					gyumolcsok[j] = gyumolcsok[j + 1];
					gyumolcsok[j + 1] = temp;
				}
			}

		}

		// kiirás,

		System.out.println("A rendezett gyümölcsök: " + Arrays.toString(gyumolcsok));

	}

}
